Rails.application.routes.draw do

  get 'admin' => 'admin#index'

  get "sessions/create"
  get "sessions/destroy"
  controller :sessions do
    get 'login' => :new
    post 'login' => :create
    delete 'logout' => :destroy
  end

  resources :users

  resources :products do
    get :who_bought, on: :member
  end

  # route scope
  scope '(:locale)' do
    resources :orders
    resources :line_items
    resources :carts
    root 'store#index', as: 'store', via: :all
  end

end
